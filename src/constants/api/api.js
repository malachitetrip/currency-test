export const API_URL = 'https://api.exchangeratesapi.io';
export const CODES = {
    RUB: 'RUB',
    USD: 'USD',
    EUR: 'EUR',
    JPY: 'JPY',
};
export const ROUTES = {
    latest: '/latest?',
};
export const QUERIES = {
    base: 'base=',
    symbols: 'symbols=',
}