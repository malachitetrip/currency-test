export const GET_CURRENCY_START = 'GET_CURRENCY_START';
export const GET_CURRENCY_SUCCESS = 'GET_CURRENCY_SUCCESS';
export const GET_CURRENCIES_START = 'GET_CURRENCIES_START';
export const GET_CURRENCIES_SUCCESS = 'GET_CURRENCIES_SUCCESS';
export const SET_ADDITIONAL_CURRENCY = 'SET_ADDITIONAL_CURRENCY';
export const RESET_CURRENSIES = 'RESET_CURRENSIES';