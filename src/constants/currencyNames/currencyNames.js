export const NAMES = {
    EUR: 'Euro',
    USD: 'US dollar',
    JPY: 'Japanese yen',
    CAD: 'Canadian dollar',
}