import { takeEvery, put, call, delay, select } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import { GET_CURRENCY_START, GET_CURRENCIES_START } from '../constants/actionTypes/currency';
import { getCurrenciesSuccess, getCurrencySuccess } from '../actions/currency';
import { ROUTES, CODES, QUERIES } from '../constants/api/api';
import Api from '../utils/api/api';

const getSymbols = items => {
    let symbols = '';
    items.forEach((element, idx) => {
        if (idx === 0) {
            symbols = `${element}`; 
        } else {
            symbols += `,${element}`;
        }
    });
    return symbols;
}

const getSymbolsList = state => state.currency.symbols;

function* requestCurrencies() {
    try {
        const symbols = yield select(getSymbolsList);
        const response = yield call(Api.get, `${ROUTES.latest}${QUERIES.base}${CODES.RUB}&${QUERIES.symbols}${getSymbols(symbols)}`);
        yield put(getCurrenciesSuccess(response.data.rates));
    } catch (error) {
        console.error(error);
    }
}

function* requestCurrency(action) {
    try {
        const response = yield call(Api.get, `${ROUTES.latest}${QUERIES.base}${CODES.RUB}&${QUERIES.symbols}${action.payload}`);
        yield delay(1000);
        yield put(getCurrencySuccess(response.data.rates));
    } catch (error) {
        console.error(error);
    }
}

function countdown(secs) {
    return eventChannel(emitter => {
        const iv = setInterval(() => {
            secs -= 1;
            if (secs > 0) {
                emitter(secs);
            } else {
                emitter(10);
            }
        }, 10000);
        return () => {
            clearInterval(iv);
        };
    });
};


export default function* watchSaga() {
    yield takeEvery(GET_CURRENCIES_START, requestCurrencies);
    yield takeEvery(GET_CURRENCY_START, requestCurrency);
    const channel = yield call(countdown, 10);
    yield takeEvery(channel, requestCurrencies); 
}