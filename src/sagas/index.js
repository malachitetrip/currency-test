import { all, fork } from 'redux-saga/effects';

import Currencies from './currencies';

const sagas = [
    Currencies,
];

export default function* root() {
    yield all(sagas.map(saga => fork(saga)));
}
