import { 
    GET_CURRENCIES_START,
    GET_CURRENCIES_SUCCESS,
    GET_CURRENCY_START,
    GET_CURRENCY_SUCCESS,
    SET_ADDITIONAL_CURRENCY,
    RESET_CURRENSIES,
} from '../constants/actionTypes/currency';

export const getCurrenciesStart = data => ({
    type: GET_CURRENCIES_START,
    payload: data,
})

export const getCurrenciesSuccess = data => ({
    type: GET_CURRENCIES_SUCCESS,
    payload: data,
});

export const getCurrencyStart = data => ({
    type: GET_CURRENCY_START,
    payload: data,
})

export const getCurrencySuccess = data => ({
    type: GET_CURRENCY_SUCCESS,
    payload: data,
})

export const setAdditionalSymbol = data => ({
    type: SET_ADDITIONAL_CURRENCY,
    payload: data,
})

export const resetCurrencies = () => ({
    type: RESET_CURRENSIES,
})