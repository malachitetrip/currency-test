import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router';
import { Provider } from 'react-redux';

import './styles/scss/global.scss';

import App from './App';
import store from './store';

ReactDOM.render(
    <Provider store={ store }>
        <BrowserRouter>
            <Route component={ App } />
        </BrowserRouter>
    </Provider>,
    document.getElementById('App'),
);