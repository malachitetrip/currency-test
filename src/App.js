import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import { connect } from 'react-redux';

import Currency from './pages/Currency';

const mapStateToProps = state => state;

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" component={ Currency } />
      </Switch>
    );
  }
}

export default connect(mapStateToProps)(App);
