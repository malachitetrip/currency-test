import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

import IconRefresh from '../../Atoms/Icons/Refresh';

import './styles.scss';

class TableItem extends PureComponent {
    render() {
        const { name, code, result, exchange, callBack, loading } = this.props;
        const iconClass = `icon ${loading ? 'rotate' : ''}`;

        return (
            <Fragment>
                <div className="first-box">
                    <div className="row">
                        <div className="cell">
                            <span className="text">{ name }</span>
                        </div>
                        <div className="cell">
                            <span className="text">{ code }</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="cell">
                            <span className="text">{ result }</span>
                        </div>
                        <div className="cell">
                            <span className="text">{ exchange }</span>
                        </div>
                    </div>
                </div>
                <div className="second-box">
                    {
                        callBack && 
                            <div className="refresh" onClick={ () => callBack(code) } >
                                <IconRefresh className={ iconClass } />
                            </div> 
                    }
                    {
                        !callBack && 
                            <span className="text">refresh</span>
                    }
                </div>
            </Fragment>
        ) 
    }
}

TableItem.defaultProps = {
    callBack: null,
    loading: false,
};

TableItem.propTypes = {
    name: PropTypes.string.isRequired,
    code: PropTypes.string.isRequired,
    result: PropTypes.string.isRequired,
    exchange: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired,
    callBack: PropTypes.func,
 };

export default TableItem;