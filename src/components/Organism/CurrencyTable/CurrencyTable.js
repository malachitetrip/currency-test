import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

import TableItem from '../../Molecules/TableItem';

import { 
    getCurrencyStart,
} from '../../../actions/currency';

class CurrencyTable extends PureComponent {
    render() {
        const { list, inputValue, callBack } = this.props;

        return (
            <div className="table">
                <div className="table__item">
                    <TableItem 
                        name="name" 
                        code="code" 
                        result="result" 
                        exchange="exchange"
                    />
                </div>
                {
                    list.map((e, idx) => (
                        <div className="table__item" key={ idx }>
                            <TableItem 
                                name={ e.name } 
                                code={ e.code } 
                                loading={ e.refresh }
                                result={ parseFloat(inputValue * e.value).toFixed(4) } 
                                exchange={ parseFloat(e.value).toFixed(4) }
                                callBack={ callBack } 
                            />
                        </div>
                    ))
                }
            </div>
        )
    }
};

CurrencyTable.defaultProps = {
    inputValue: 0,
};

CurrencyTable.propTypes = {
    list: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        code: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired,
        refresh: PropTypes.bool.isRequired,
    })).isRequired,
    callBack: PropTypes.func.isRequired,
    inputValue: PropTypes.string,
 };

export default CurrencyTable;