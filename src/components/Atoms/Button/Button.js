import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

class Button extends PureComponent {
    render() {
        const { code, title, callBack } = this.props;

        return (
            <div 
                className="button" 
                onClick={ () => callBack(code) }
            >
                <span className="title">{ title }</span>
            </div>
        ) 
    }
}

Button.defaultProps = {
    code: null,
    callBack: null,
};

Button.propTypes = {
    title: PropTypes.string.isRequired,
    code: PropTypes.string,
    callBack: PropTypes.func,
 };

export default Button;