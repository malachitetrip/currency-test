import { 
    GET_CURRENCIES_SUCCESS, 
    GET_CURRENCY_SUCCESS, 
    GET_CURRENCY_START,
    SET_ADDITIONAL_CURRENCY,
    RESET_CURRENSIES,
} from '../constants/actionTypes/currency';
import { NAMES } from '../constants/currencyNames/currencyNames';

const initialState = {
    list: [],
    symbols: ['USD', 'EUR'],
};

const formatCurrency = item => {
    return { code: item[0], value: item[1], name: NAMES[item[0]], refresh: false };
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CURRENCIES_SUCCESS: {
            const list = Object.entries(action.payload).map(e => formatCurrency(e));
            return { ...state, list };
        }

        case GET_CURRENCY_START: {
            const item = state.list.find(e => e.code === action.payload);
            const itemIdx = state.list.indexOf(item);
            const listFirstPart = state.list.slice(0, itemIdx);
            const listSecondPart = state.list.slice(itemIdx + 1);
            const newItem = { ...item, refresh: true }; 
            return { ...state, list: [...listFirstPart, newItem, ...listSecondPart] };
        }

        case GET_CURRENCY_SUCCESS: {
            const item = state.list.find(e => e.code === Object.keys(action.payload)[0]);
            const itemIdx = state.list.indexOf(item);
            const listFirstPart = state.list.slice(0, itemIdx);
            const listSecondPart = state.list.slice(itemIdx + 1);
            const newItem = { ...item, refresh: false, value: Object.values(action.payload)[0] };
            return { ...state, list: [...listFirstPart, newItem, ...listSecondPart] };
        }

        case SET_ADDITIONAL_CURRENCY: {
            const symbols = [action.payload].concat(state.symbols);
            return { ...state, symbols };
        }

        case RESET_CURRENSIES: {
            return initialState;
        }

        default:
            return state;
    }
};
