import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import { onlyNums } from '../../utils/validation/validation';
import { 
    getCurrenciesStart, 
    setAdditionalSymbol,
    resetCurrencies,
    getCurrencyStart,
} from '../../actions/currency';

import './styles.scss';

import Button from '../../components/Atoms/Button';
import CurrencyTable from '../../components/Organism/CurrencyTable';

const mapStateToProps = ({ currency }) => ({
    currency,
});

const mapDispatchToProps = {
    dispatchGetCurrenciesStart: getCurrenciesStart,
    dispatchSetAdditionalSymbol: setAdditionalSymbol,
    dispatchResetCurrencies: resetCurrencies,
    dispatchGetCurrencyStart: getCurrencyStart,
};

class Currency extends Component {
    state = {
        inputValue: '',
        addList: [
            {
                name: 'Canadian Dollar',
                code: 'CAD',
                added: false,
            },
            {
                name: 'Japanese Yen',
                code: 'JPY',
                added: false,
            },
        ],
    }

    componentDidMount() {
        const { dispatchGetCurrenciesStart } = this.props;
        dispatchGetCurrenciesStart();
    }

    addCurrency = code => {
        const { addList } = this.state;
        const { dispatchGetCurrenciesStart, dispatchSetAdditionalSymbol } = this.props;
        addList.find(e => e.code === code).added = true;
        this.setState({ addList });
        dispatchSetAdditionalSymbol(code);
        dispatchGetCurrenciesStart();       
    }

    resetCurrencies = () => {
        const { dispatchResetCurrencies, dispatchGetCurrenciesStart } = this.props;
        const { addList } = this.state;
        addList.forEach(e => e.added = false);
        this.setState({ addList });
        dispatchResetCurrencies();
        dispatchGetCurrenciesStart();
    }

    refreshCurrency = code => {
        const { dispatchGetCurrencyStart } = this.props;
        dispatchGetCurrencyStart(code);
    }

    inputChange = inputValue => {
        if (!onlyNums(inputValue)) return;
        this.setState({ inputValue });
    } 

    render() {
        const { currency } = this.props;
        const { inputValue, addList } = this.state;

        return (
            <div className="page">
                <div className="input-box">
                    <input 
                        type="text" 
                        value={ inputValue }
                        onChange={ e => this.inputChange(e.target.value) } 
                        className="input"
                        placeholder="Enter RUB amount"
                    />
                </div>
                {
                    currency.list[0] && 
                        <CurrencyTable inputValue={ inputValue } list={ currency.list } callBack={ this.refreshCurrency } />
                }
                <div className="buttons">
                    {
                        addList.map(e => (
                            !e.added && 
                                <Fragment key={ e.code }>
                                    <Button 
                                        callBack={ this.addCurrency } 
                                        title={ `add ${e.name}` }
                                        code={ e.code }
                                    />
                                </Fragment>
                        ))
                    }
                    {
                        addList.every(e => e.added) && 
                            <Button 
                                callBack={ this.resetCurrencies } 
                                title="reset"
                            />
                    }
                </div>
            </div>
        )
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Currency);